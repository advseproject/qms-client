// @flow

const configuration = {
  backend: "http://qms-backend.myicpc.live",
  keycloak: "http://ec2-3-87-186-137.compute-1.amazonaws.com:8080/auth",
  dashboard: "http://ums-demo.myicpc.live",
  profileUrl: "http://ums-demo.myicpc.live/profile"
};

export default configuration;
